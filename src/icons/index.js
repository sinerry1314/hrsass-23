import Vue from 'vue'
// 导入全局的Svg图标组件
import SvgIcon from '@/components/SvgIcon'

// 全局注册Svg图标组件
Vue.component('svg-icon', SvgIcon)

// 如下3行代码的意思：
// 把svg目录下所有的svg图标全部以模块的方式打包到index.html中
// 这样就可以用过svg-icon组件使用svg目录下的任何svg图标
const req = require.context('./svg', false, /\.svg$/)
const requireAll = requireContext => requireContext.keys().map(requireContext)
requireAll(req)
