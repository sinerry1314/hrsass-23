import request from '@/utils/request'

/**
 * 获取权限列表
 * @returns Promise
 */
export const getPermListAPI = () => {
  return request({
    method: 'get',
    url: '/sys/permission'
  })
}

/**
 * 新增权限
 * @param {Object} permForm 权限表单
 * @returns Promise
 */
export const addPermAPI = (permForm) => {
  return request({
    method: 'post',
    url: '/sys/permission',
    data: permForm
  })
}

/**
 * 获取权限详情
 * @param {String} id 权限id
 * @returns Promise
 */
export const getPermDetailAPI = (id) => {
  return request({
    method: 'get',
    url: `/sys/permission/${id}`
  })
}

/**
 * 编辑权限
 * @param {Object} permForm 权限表单
 * @returns Promise
 */
export const editPermAPI = (permForm) => {
  return request({
    method: 'put',
    url: `/sys/permission/${permForm.id}`,
    data: permForm
  })
}

/**
 * 删除权限
 * @param {String} id 权限id
 * @returns Promise
 */
export const delPermAPI = (id) => {
  return request({
    method: 'delete',
    url: `/sys/permission/${id}`
  })
}
