import request from '@/utils/request'

/**
 * 登录
 * @param {Object} loginForm 登录表单
 * @returns Promise
 */
export const loginAPI = (loginForm) => {
  return request({
    method: 'post',
    url: '/sys/login',
    data: loginForm
  })
}

/**
 * 获取用户基本资料(目的是得到用户Id)
 * @returns Promise
 */
export const getUserProfileAPI = () => {
  return request({
    method: 'post',
    url: '/sys/profile'
  })
}

/**
 * 根据用户Id获取用户信息
 * @param {String} userId 用户Id
 * @returns Promise
 */
export const getUserInfoAPI = (userId) => {
  return request({
    method: 'get',
    url: `/sys/user/${userId}`
  })
}
