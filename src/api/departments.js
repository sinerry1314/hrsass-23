import request from '@/utils/request'

/**
 * 获取部门列表
 * @returns Promise
 */
export const getDeptListAPI = () => {
  return request({
    method: 'get',
    url: '/company/department'
  })
}

/**
 * 删除部门
 * @param {String} deptId 部门id
 * @returns Promise
 */
export const delDeptAPI = (deptId) => {
  return request({
    method: 'delete',
    url: `/company/department/${deptId}`
  })
}

/**
 * 获取负责人列表
 * @returns Promise
 */
export const getManagerListAPI = () => {
  return request({
    method: 'get',
    url: '/sys/user/simple'
  })
}

/**
 * 新增部门
 * @param {Object} deptForm 部门表单
 * @returns Promise
 */
export const addDeptAPI = (deptForm) => {
  return request({
    method: 'post',
    url: '/company/department',
    data: deptForm
  })
}

/**
 * 获取部门详情
 * @param {String} deptId 部门id
 * @returns Promise
 */
export const getDeptDetailAPI = (deptId) => {
  return request({
    method: 'get',
    url: `/company/department/${deptId}`
  })
}

/**
 * 编辑部门
 * @param {Object} deptForm 部门表单
 * @returns Promise
 */
export const editDeptAPI = (deptForm) => {
  return request({
    method: 'put',
    url: `/company/department/${deptForm.id}`,
    data: deptForm
  })
}
