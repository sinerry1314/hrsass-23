import request from '@/utils/request'

/**
 * 获取员工列表
 * @param {Object} query 查询参数 { page: 1, size: 5 }
 * @returns Promise
 */
export const getEmployeeListAPI = (query) => {
  return request({
    method: 'get',
    url: '/sys/user',
    params: query
  })
}

/**
 * 删除员工
 * @param {String} employeeId 员工id
 * @returns Promise
 */
export const delEmployeeAPI = (employeeId) => {
  return request({
    method: 'delete',
    url: `/sys/user/${employeeId}`
  })
}

/**
 * 新增员工(单个)
 * @param {Object} employeeForm 员工表单
 * @returns Promise
 */
export const addEmployeeAPI = (employeeForm) => {
  return request({
    method: 'post',
    url: '/sys/user',
    data: employeeForm
  })
}

/**
 * 批量新增员工
 * @param {Array} employeeList 员工列表
 * @returns Promise
 */
export const addEmployeeBatchAPI = (employeeList) => {
  return request({
    method: 'post',
    url: '/sys/user/batch',
    data: employeeList
  })
}

/**
 * 获取员工信息
 * @param {String} employeeId 员工id
 * @returns Promise
 */
export const getEmployeeInfoAPI = (employeeId) => {
  return request({
    method: 'get',
    url: `/sys/user/${employeeId}`
  })
}

/**
 * 编辑员工信息
 * @param {Object} employeeForm 员工表单
 * @returns Promise
 */
export const updateEmployeeAPI = (employeeForm) => {
  return request({
    method: 'put',
    url: `/sys/user/${employeeForm.id}`,
    data: employeeForm
  })
}

/**
 * 给员工分配角色
 * @param {String} id 员工id
 * @param {Array} roleIds 角色id数组
 * @returns Promise
 */
export const assignRoleAPI = (id, roleIds) => {
  return request({
    method: 'put',
    url: '/sys/user/assignRoles',
    data: {
      id,
      roleIds
    }
  })
}
