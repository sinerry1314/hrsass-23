import request from '@/utils/request'

/**
 * 获取角色列表
 * @param {Object} query 查询参
 * @returns Promise
 */
export const getRoleListAPI = (query) => {
  return request({
    method: 'get',
    url: '/sys/role',
    params: query
  })
}

/**
 * 删除角色
 * @param {String} roleId 角色id
 * @returns Promise
 */
export const delRoleAPI = (roleId) => {
  return request({
    method: 'delete',
    url: `/sys/role/${roleId}`
  })
}

/**
 * 新增角色
 * @param {Object} roleForm 角色表单
 * @returns Promise
 */
export const addRoleAPI = (roleForm) => {
  return request({
    method: 'post',
    url: '/sys/role',
    data: roleForm
  })
}

/**
 * 获取角色详情
 * @param {String} roleId 角色id
 * @returns Promise
 */
export const getRoleDetailAPI = (roleId) => {
  return request({
    method: 'get',
    url: `/sys/role/${roleId}`
  })
}

/**
 * 编辑角色
 * @param {Object} roleForm 角色表单
 * @returns Promise
 */
export const editRoleAPI = (roleForm) => {
  return request({
    method: 'put',
    url: `/sys/role/${roleForm.id}`,
    data: roleForm
  })
}

/**
 * 获取公司详情
 * @param {String} companyId 公司id
 * @returns Promise
 */
export const getCompanyDetailAPI = (companyId) => {
  return request({
    method: 'get',
    url: `/company/${companyId}`
  })
}

/**
 * 给角色分配权限
 * @param {String} id 角色id
 * @param {Array} permIds 权限数组
 * @returns Promise
 */
export const assignPermAPI = (id, permIds) => {
  return request({
    method: 'put',
    url: '/sys/role/assignPrem',
    data: {
      id,
      permIds
    }
  })
}
