import defaultSettings from '@/settings'

const { showSettings, fixedHeader, sidebarLogo } = defaultSettings

// 默认主题色
const defaultThemeColor = '#3d6df8'

const state = {
  showSettings: showSettings,
  fixedHeader: fixedHeader,
  sidebarLogo: sidebarLogo,
  // 全局主题色
  theme: defaultThemeColor
}

const mutations = {
  CHANGE_SETTING: (state, { key, value }) => {
    // hasOwnProperty()是对象的一个方法，判断对象自身是否拥有某个属性(不会沿着原型链往上查找)
    // 如果返回true；否则返回false
    // eslint-disable-next-line no-prototype-builtins
    if (state.hasOwnProperty(key)) {
      state[key] = value
    }
  },
  // 重置主题色
  resetTheme: (state) => {
    state.theme = defaultThemeColor
  }
}

const actions = {
  changeSetting ({ commit }, data) {
    commit('CHANGE_SETTING', data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

