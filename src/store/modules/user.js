import { resetRouter } from '@/router'
import {
  getToken,
  setToken,
  removeToken
} from '@/utils/auth'
// 导入登录、获取用户信息接口函数
import {
  loginAPI,
  getUserProfileAPI,
  getUserInfoAPI
} from '@/api/user'

const state = () => {
  return {
    // 用户令牌
    // 刷新页面的时候，从本地取token，借助逻辑或短路
    token: getToken() || '',
    // 用户信息
    userInfo: {}
  }
}

const mutations = {
  // 设置token
  setToken: (state, token) => {
    state.token = token
    // 本地在存一份token
    setToken(token)
  },
  // 设置用户信息
  setUserInfo: (state, userInfo) => {
    state.userInfo = userInfo
  },
  // 清空token
  removeToken: (state) => {
    state.token = ''
    // 清空本地的token
    removeToken()
  },
  // 清空用户信息
  removeUserInfo: (state) => {
    state.userInfo = {}
  }
}

const actions = {
  // 登录action
  loginAction: async ({ commit }, loginForm) => {
    // 调接口、发登录请求
    const resp = await loginAPI(loginForm)
    // 提交mutation
    commit('setToken', resp.data)
  },
  // 获取用户信息action
  getUserInfoAction: async ({ commit }) => {
    // 调接口发请求
    const { data: userProfile } = await getUserProfileAPI()
    // 基于此次接口返回的userId继续发请第2次请求(获取用户头像等信息)
    const { data: userInfo } = await getUserInfoAPI(userProfile.userId)
    // 合并
    const userObj = { ...userProfile, ...userInfo }
    // { ...userProfile, ...userInfo }两个对象合并之后作为参数传给mutation函数
    // 提交mutation
    commit('setUserInfo', userObj)
    // 返回userObj
    return userObj
  },
  // 退出登录action
  logoutAction: ({ commit }) => {
    commit('removeToken')
    commit('removeUserInfo')
    // 重置路由实例（把路由表中所有所有信息删除）
    resetRouter()
    // 清空vuex中路由数组
    commit('route/removeRoutes', null, {
      // 从全局出发
      root: true
    })
    // 重置主题色
    commit('settings/resetTheme', null, {
      root: true
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
