// 导入路由模块下的 静态路由表
import { constantRoutes, asyncRoutes } from '@/router'

const state = () => {
  return {
    // 路由表数组：初始值是静态路由表的值
    routes: constantRoutes
  }
}

const mutations = {
  // 设置路由表
  setRoutes: (state, showAsyncRoutes) => {
    // 给routes数组追加筛选后的动态路由表
    // state.routes.push(...showAsyncRoutes)
    // 或
    state.routes = [
      ...constantRoutes,
      ...showAsyncRoutes
    ]
  },
  // 删除路由表
  removeRoutes: (state) => {
    state.routes = []
  }
}

const actions = {
  // 筛选动态路由表
  filterAsyncRouteAction: ({ commit }, menus) => {
    // 基于动态路由表和menus数组筛选
    const showAsyncRoutes = asyncRoutes
      .filter(item => menus.includes(item.children[0].name))
    // 提交mutation，传入筛选结果作为参数
    commit('setRoutes', showAsyncRoutes)
    // 返回筛选结果(给路由守卫那边用)
    return showAsyncRoutes
  }
}

export default {
  // 开启命名空间
  namespaced: true,
  state,
  mutations,
  actions
}
