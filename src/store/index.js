// 导入 Vue 构造函数
import Vue from 'vue'
// 导入 Vuex 构造函数
import Vuex from 'vuex'

// 导入全局 getters
import getters from './getters'

// 导入4个子模块
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import route from './modules/route'
import tagsView from './modules/tagsView'
// 导入让 vuex 数据持久化的插件
import createPersisitedState from 'vuex-persistedstate'
Vue.use(Vuex)

const store = new Vuex.Store({
  // 注册4个子模块（采用对象简写语法）
  modules: {
    app,
    settings,
    user,
    route,
    tagsView
  },
  getters,
  plugins: [
    // 使用让 vuex 数据持久化的插件，传入配置
    createPersisitedState({
      // 本地存储键名 -> localStorage.setItem('hrsass-23', vuex下的state数据)
      key: 'hrsass-23',
      // 哪些模块下的数据需要持久化
      // 使用这个插件的好处：
      // 1. 自动把vuex中的数据同步到localStorage一份
      // 2. 当我们刷新页面的时候，自动的把localStorage的数据赋值给vuex中的数据
      // 3. 每当vuex中的数据发生变了，也会自动地把最新的vuex数据同步到localStorage
      // 数据的使用方式：
      // 1. 代码中，我们都是从vuex中做数据做增删改查（只用vuex中的数据）
      // 2. localStorage只负责刷新数据不丢失的任务
      paths: ['app', 'settings']
    })
  ]
})

export default store

// token为什么同时用vuex和本地存储共同管理？
// 1. 响应式 2. 读写速度快 3.刷新不丢失

// 存储在vuex中数据的优点和缺点：
// 优点：
//    1. 响应式
//    2. 读写速度快（从内存中直接读写）
// 缺点：1. 刷新会丢失

// 存储在localStorage中数据的优点和缺点
// 优点：刷新不丢失
// 缺点：1. 非响应式  2. 读写速度慢(从磁盘中读写，操作文件慢)
