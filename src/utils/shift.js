// 数组转树
// arr: 待转换的数组
// id: 查找有没有子部门的id
export const arr2Tree = (arr, id) => {
  const result = []
  // 遍历arr数组
  for (let i = 0; i < arr.length; i++) {
    // 找到所有一级部门
    if (arr[i].pid === id) {
      // 函数递归，查找子部门
      const childs = arr2Tree(arr, arr[i].id)
      // 如果childs数组不为空，则说明当前部门有子部门
      if (childs.length) {
        // 有子部门就需要给当期部门添加children子部门
        arr[i].children = childs
      }
      // 把一级部门加入数组
      result.push(arr[i])
    }
  }
  return result
}
