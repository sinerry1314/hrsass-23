// 导入 axios 模块
import axios from 'axios'
// 导入 store 模块
import store from '@/store'
// 导入 router 模块
import router from '@/router'
// 导入 Message 消息提示函数
import { Message } from 'element-ui'

// 创建 axios 实例
const service = axios.create({
  // 基地址(自动从环境变量下获取基地址)
  baseURL: process.env.VUE_APP_BASE_API,
  // 超时时间
  timeout: 5000
})

// 请求拦截器
service.interceptors.request.use(config => {
  // config: 请求大对象，{method，url，params，data，headers，baseURL}
  // 从vuex中获取token
  const { token } = store.state.user
  if (token) { // 如果有token
    // 通过请求头携带给后台，给headers添加Authorization属性
    config.headers.Authorization = `Bearer ${token}`
  }
  // 必须返回请求对象给到服务器
  return config
}, err => Promise.reject(err))

// 响应拦截器
service.interceptors.response.use(resp => {
  // 解构
  const { data } = resp
  // 判断 data.success
  if (data.success) { // 请求成功
    // 直接返回data数据
    return data
  }
  // 请求失败，返回一个失败态的Promise实例
  return Promise.reject(data)
}, async err => {
  // 判断token过期状态码
  if (err.response.status === 401 &&
    err.response.data.code === 10002) {
    // 给出token过期提示
    Message.error('登录过期、请重新登录')
    // 清空vuex中的token和用户信息
    await store.dispatch('user/logoutAction')
    // 跳转至登录，并携带回跳地址
    router.replace(`/login?redirectUrl=${router.currentRoute.fullPath}`)
  }
  return Promise.reject(err)
})

// 导出请求实例
export default service
