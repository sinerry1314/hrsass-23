// 导入 Vue 构造函数
import Vue from 'vue'
// 导入 Router 构造函数
import Router from 'vue-router'
// 导入 Layout 架子（基础结构）
import Layout from '@/layout'
// 导入 8 个动态路由规则对象
import approvalsRouter from './modules/approvals'
import attendancesRouter from './modules/attendances'
import departmentsRouter from './modules/departments'
import employeesRouter from './modules/employees'
import permissionsRouter from './modules/permissions'
import salarysRouter from './modules/salarys'
import settingsRouter from './modules/settings'
import social_securitys from './modules/social_securitys'
Vue.use(Router)

// 动态路由表数组(需要权限控制的)
export const asyncRoutes = [
  departmentsRouter,
  employeesRouter,
  settingsRouter,
  permissionsRouter,
  approvalsRouter,
  attendancesRouter,
  salarysRouter,
  social_securitys
]

// 注册路由插件
// 静态路由表（所有角色都可访问的路由）
export const constantRoutes = [
  { // 登录
    path: '/login',
    component: () => import('@/views/login/index'),
    // hidden: true表示当前路由不会出现在主页的左侧菜单
    hidden: true
  },

  { // 404
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  { // Layout
    path: '/',
    component: Layout,
    // 重定向
    redirect: '/dashboard',
    children: [
      { // 首页
        path: 'dashboard',
        name: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        // title: 菜单名称
        // icon:  菜单小图标
        meta: { title: '首页', icon: 'dashboard' }
      }
    ]
  }
  // 404 page must be placed at the end !!!
  // { path: '*', redirect: '/404', hidden: true }
]

// 创建路由实例，把 new VueRouter() 写在了函数中
const createRouter = () => new Router({
  // 路由模式，默认是hash模式，可选值还有history历史模式
  // mode: 'history',
  // 当路由切换的时候，让页面回到最顶部
  scrollBehavior: () => ({ y: 0 }),
  // 临时让静态路由表和动态路由表合并(当下用户可以看到所有菜单)
  // routes: [...constantRoutes, ...asyncRoutes]
  // 写死的只开放静态路由表
  routes: constantRoutes
})

// 调用创建路由实例函数，接收路由实例
const router = createRouter()

// 重置路由函数
export function resetRouter () {
  // 删除所有所有表信息
  const newRouter = createRouter()
  router.matcher = newRouter.matcher
}

// 导出 router 实例，提供给其它js模块用
export default router
