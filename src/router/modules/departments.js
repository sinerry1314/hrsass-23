import Layout from '@/layout'
// 组织架构模块路由规则
export default {
  path: '/departments',
  component: Layout,
  children: [
    { // path为空字符串表示默认路由
      path: '',
      // name作为后续动态路由表筛选的条件
      name: 'departments',
      component: () => import('@/views/departments'),
      meta: {
        title: '组织架构',
        icon: 'tree'
      }
    }
  ]
}
