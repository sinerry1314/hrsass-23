import Layout from '@/layout'
// 社保模块路由规则
export default {
  path: '/social-securitys',
  component: Layout,
  children: [
    { // path为空字符串表示默认路由
      path: '',
      // name作为后续动态路由表筛选的条件
      name: 'social_securitys',
      component: () => import('@/views/social_securitys'),
      meta: {
        title: '社保',
        icon: 'table'
      }
    }
  ]
}
