import Layout from '@/layout'
// 工资模块路由规则
export default {
  path: '/salarys',
  component: Layout,
  children: [
    { // path为空字符串表示默认路由
      path: '',
      // name作为后续动态路由表筛选的条件
      name: 'salarys',
      component: () => import('@/views/salarys'),
      meta: {
        title: '工资',
        icon: 'money'
      }
    }
  ]
}
