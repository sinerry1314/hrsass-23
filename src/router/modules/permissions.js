import Layout from '@/layout'
// 权限管理模块路由规则
export default {
  path: '/permissions',
  component: Layout,
  children: [
    { // path为空字符串表示默认路由
      path: '',
      // name作为后续动态路由表筛选的条件
      name: 'permissions',
      component: () => import('@/views/permissions'),
      meta: {
        title: '权限管理',
        icon: 'lock'
      }
    }
  ]
}
