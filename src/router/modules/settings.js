import Layout from '@/layout'
// 公司设置模块路由规则
export default {
  path: '/settings',
  component: Layout,
  children: [
    { // path为空字符串表示默认路由
      path: '',
      // name作为后续动态路由表筛选的条件
      name: 'settings',
      component: () => import('@/views/settings'),
      meta: {
        title: '公司设置',
        icon: 'setting'
      }
    }
  ]
}
