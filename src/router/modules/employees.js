import Layout from '@/layout'
// 员工模块路由规则
export default {
  path: '/employees',
  component: Layout,
  children: [
    { // path为空字符串表示默认路由
      path: '',
      // name作为后续动态路由表筛选的条件
      name: 'employees',
      component: () => import('@/views/employees'),
      meta: {
        title: '员工',
        icon: 'people'
      }
    }, {
      path: 'upload-excel',
      component: () => import('@/views/excel'),
      hidden: true
    }, {
      path: ':id',
      component: () => import('@/views/employees/detail.vue'),
      hidden: true
    }, {
      path: 'print/:id',
      component: () => import('@/views/employees/print.vue'),
      hidden: true
    }
  ]
}
