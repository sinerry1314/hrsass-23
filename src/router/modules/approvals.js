import Layout from '@/layout'
// 审批模块路由规则
export default {
  path: '/approvals',
  component: Layout,
  children: [
    { // path为空字符串表示默认路由
      path: '',
      // name作为后续动态路由表筛选的条件
      name: 'approvals',
      component: () => import('@/views/approvals'),
      meta: {
        title: '审批',
        icon: 'tree-table'
      }
    }
  ]
}
