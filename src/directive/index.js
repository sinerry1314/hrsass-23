import Vue from 'vue'
import store from '@/store'

Vue.directive('imgerror', {
  // inserted: 当指令所在元素插入到真实DOM中自动执行一次
  // el: 指令所在DOM元素
  // binding: 指令信息对象
  // binding.value: 获取指令绑定表达式的值(在这里就是默认头像地址)
  inserted (el, binding) {
    // 监听el的error错误事件
    el.addEventListener('error', () => {
      // 用默认头像地址给el的src赋值
      el.src = binding.value
    })
  }
})

// 自定义权限点指令
Vue.directive('permission', {
  inserted (el, binding) {
    // 拿到vuex中userInfo下points数组
    const { points } = store.state.user.userInfo.roles
    // 判断binding.value在不在points数组中
    const flag = points.includes(binding.value)
    if (!flag) { // 如果不在数组中
      // 从DOM树上移除
      el.remove()
    }
  }
})
