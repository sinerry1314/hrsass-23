module.exports = {

  // 控制网页标题
  title: 'hrsass-23人资系统',

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  // 主页头部是否固定定位
  fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  // 主页左侧菜单顶部是否显示logo图片
  sidebarLogo: true
}
